import subprocess
import sys
from testrail import *
import json
import datetime
import argparse
import shlex
import yaml
import os
from pprint import pprint

scriptName = "BUILDSERVER"


#############################################
#
#
#
###############################################


##########
# avoir les plans de la dernieres semaine , si on est toujours dans la meme semain OK
# Sinon nouveau plan
# IL FAUT FILTER ENFONCTION DE LA DATE
##############################################################
# creer un plan avec un nom en fctn de la date
########################################################""""
def createPlan(idProject, dateValue):
    date = dateValue
    tmpProject = str(idProject)
    tmpCreatedPlan = client.send_post("add_plan/" + tmpProject, {
        "name": "Plan of " + date,
        "description": "TEST CI AUTO"
    }
                                      )

    createdPlan = tmpCreatedPlan['id']
    return createdPlan;


##############################################################
# creer un Run avec un nom en fonction de la date
########################################################""""
def createRun(idProject, dateValue):
    date = dateValue
    tmpIdProject = str(idProject)
    runCreated = client.send_post("add_run/" + tmpIdProject,
                                  {
                                      "name": "Run of " + date,
                                      "include_all": True
                                  }
                                  )
    idRun = runCreated["id"]
    return idRun;


########################
def createRunWithPlan(iDProject, argCmdBash, idSuite):
    try:
        dateOfTheDay = dateCreation()
        tmpIdProject = str(iDProject)
        plans = client.send_get("get_plans/" + tmpIdProject)
        tmpSuiteID = client.send_get(("get_plan/" + str(plans[0]['id'])))
        # suiteId = tmpSuiteID['entries'][0]['suite_id']

    # IdPlans = plans[0]['name']  # donne toujours le dernier crée
    # tmpDateOfTheDay = (re.search("(\()([0-9]+)(\))", dateOfTheDay)).group(2)
    # tmpIdPlans = (re.search("(\()([0-9]+)(\))", IdPlans)).group(2)
    except IndexError or UnboundLocalError:
        planOfTheWeek = createPlan(iDProject, dateOfTheDay)
        runOfTheDay = addRunToPlan(planOfTheWeek, 16, dateOfTheDay)
        tmpIdProject = str(iDProject)
        plans = client.send_get("get_plans/" + tmpIdProject)
        tmpSuiteID = client.send_get(("get_plan/" + str(plans[0]['id'])))
        # suiteId = tmpSuiteID['entries'][0]['suite_id']
    else:
        if argCmdBash == "Schedule":
            planOfTheWeek = createPlan(iDProject, dateOfTheDay)
            runOfTheDay = addRunToPlan(planOfTheWeek, idSuite, dateOfTheDay)
        else:
            runOfTheDay = addRunToPlan(plans[0]['id'], idSuite, dateOfTheDay)
            planOfTheWeek = None

    return planOfTheWeek, runOfTheDay;


##############################################################
# cree un tag de la date pour referencer lesRun et Plans
#
#
########################################################""""
def dateCreation():
    date = datetime.datetime.today()
    #tmpDate = date.strftime(("%m-%d-%Y %H:%M (%W)"))
    tmpDate = os.environ.get('CI_COMMIT_SHORT_SHA')

    return tmpDate;


##############################################################
# ajoute le resultat d'un test en fcontion
# du case pour un Run donnée
#
########################################################""""
def addCase(iDCase, testResult, output, idRun):
    try:
        tmpIdRun = str(idRun)
        tmpIdCase = str(iDCase)
        tmpTestId = str(testResult)
        tmpOutput = str(output)
        sortie = client.send_post("add_results_for_cases/" + tmpIdRun,
                                  {
                                      "results": [
                                          {
                                              "case_id": tmpIdCase,
                                              "status_id": tmpTestId,
                                              "comment": tmpOutput,

                                          }]}
                                  )
    except:
        sortie = None
        return sortie
    return sortie;


def whichScript(listCommand):
    newListLocal = []
    newlistBuild = []
    newListTest = []
    for dict in listCommand:
        if scriptName is "BUILDSERVER" and not dict['testFlag'] and not dict['whichScript']:
            newlistBuild.append(dict)

        elif scriptName is not "BUILDSERVER" and dict['whichScript'] and not dict['testFlag']:

            newListLocal.append(dict)
        elif scriptName is not "BUILDSERVER" and dict['testFlag']:
            newListTest.append(dict)

    return newListLocal, newlistBuild, newListTest;


def addRunToPlan(idPlan, iDSuite, date):
    tmpIdPlan = str(idPlan)
    sortie = client.send_post('add_plan_entry/' + tmpIdPlan, {
        "suite_id": iDSuite,
        "name": "Run Of " + date,
        "include_all": True, })
    return sortie['runs'][0]['id'];


def argumentParsing():
    parser = argparse.ArgumentParser(description="Build and runt test")
    # parser.add_argument('Path_Folder', type=str, help="Path of the folder's test")
    # parser.add_argument('Project_ID', type=int, help="Id_ Projet findable in TESTRAIL")
    parser.add_argument('Boolean_From_scratch_or_Not', type=str, help="..")
    # parser.add_argument('Run_ID', type=int, help="Id_ Run findable in TESTRAIL")
    args = parser.parse_args()

    return args;


def getCommandFromTestRail(iniYml, argLine):
    tmpIdProject = str(iniYml['PROJECTPARAM']['ProjectID'])
    tmpIdSuite = str(iniYml['PROJECTPARAM']['SuiteID'])
    caseTest = client.send_get('get_cases/' + tmpIdProject + '&suite_id=' + tmpIdSuite)
    # pprint(caseTest)
    listCase = []
    for dict in caseTest:
        ssh = dict['custom_ssh_command']
        script = dict['custom_local_script_or_not']
        y = json.loads(dict['custom_cdmline'])
        yy = shlex.split(y, posix=True)
        # print(dict['id'],dict['custom_arg_path_folder'],dict['custom_arg_project_id'],dict['custom_arg_schedule_var'],dict['custom_bash_env'])

        if dict['custom_ssh_command'] and dict['custom_rsync']:  # Commande Rsync en SSH sous forme de =>"command"
            arg1 = iniYml['SCRIPTPARAM']['ssh']['hostname']
            arg2 = iniYml['SCRIPTPARAM']['PATH_SOURCE_RSYNC']
            arg3 = iniYml['SCRIPTPARAM']['PATH_DEST_RSYNC']
            x = str(y + " " + arg1 + ":" + arg2 + " " + arg3)
            tmpDict = {"id": dict['id'], "command": x, "ssh": ssh, "whichScript": script,
                       "testFlag": dict['custom_test_flag']}
            listCase.append(tmpDict)
        elif dict['custom_bash_env']:  # [commande env=>[ "bash","-c","command]
            arg1 = 'bash'
            arg2 = '-c'
            newArg = [arg1, arg2, y + " " + ";env -0"]
            tmpDict = {"id": dict['id'], "command": newArg, "ssh": ssh, "whichScript": script,
                       "testFlag": dict['custom_test_flag']}
            listCase.append(tmpDict)
        elif dict['custom_ssh_command'] and not dict[
            'custom_rsync']:  # commande ssh simple lancement de script tel que buildserver => "command+ arg_Shecd"
            arg3 = argLine.Boolean_From_scratch_or_Not
            newArg = y + " " + arg3
            tmpDict = {"id": dict['id'], "command": newArg, "ssh": ssh, "whichScript": script,
                       "testFlag": dict['custom_test_flag']}
            listCase.append(tmpDict)
        elif dict['custom_test_flag'] and dict[
            'custom_ssh_command']:  # cas ou la commande est un test scrip lancé en ssh => "command"
            tmpDict = {"id": dict['id'], "command": y, "ssh": ssh, "whichScript": script,
                       "testFlag": dict['custom_test_flag']}
        else:
            tmpDict = {"id": dict['id'], "command": yy, "ssh": ssh, "whichScript": script,
                       "testFlag": dict['custom_test_flag']}
            # print(y[0]['cmd'])
            listCase.append(tmpDict)

    return listCase;


# changer cettte fonction pour avoir le run en fonction du projet de la suite et du plan
def getActualRuniD(idProject):
    tmpIdProject = str(idProject)
    idPlan = client.send_get('get_plans/' + tmpIdProject)
    runId = client.send_get('get_plan/' + str(idPlan[0]['id']))
    x = range(len(runId['entries']))
    actualRunId = runId['entries'][x.stop - 1]['runs'][0]['id']
    return actualRunId;


#def sshRemoteCommand(command):
 #   client = paramiko.SSHClient()
  #  client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
   # client.connect(hostname=HOSTNAME, username=USER, port=1604, key_filename=pkey_path)

    #stdin, stdout, stderr = client.exec_command(command, bufsize=4096)

   # while not stdout.channel.exit_status_ready():
    #    for line in iter(stdout.readline, ""):
     #       print(line, end="")

      #  if stdout.channel.recv_exit_status() != 0:
       #     print("error in SSH COMMAND: =>", command)
        #    codeRetour = 1
        #else:
         #   codeRetour = 0
          #  client.close()

    #return codeRetour;


def mainCommandV2(command, iDRun):
    try:

        output = subprocess.Popen(command['command'], stdin=subprocess.PIPE, stdout=subprocess.PIPE,stderr=subprocess.STDOUT,universal_newlines=True)
        while output.poll() is None:
            out = output.stdout.read(1)
            sys.stdout.write(out)
            sys.stdout.flush()
            return_code = output.poll()

    except OSError as e:
        error = e
        print(error.args[0], error.args[1])
        print(e)
        CheckCommand = False

    else:
        if return_code == 0:
            out = "\n Command(from buildScript.py) => ", command[
                'command'], "\n SUCCEED , with return code = ", return_code
            print(out)
            checkCommand = True
            addCase(command['id'], 1, out, iDRun)
        else:
            out = "\n Command (from buildScript.py)=> ", command[
                'command'], "\n FAILED , with return code = ", return_code
            print("Command (from buildScript.py)=> ", command['command'], "\n FAILED , with return code = ",
                  return_code)
            checkCommand = False
            addCase(command['id'], 5, out, iDRun)
    return checkCommand;



def createWorkspace(path, dir, bool):
    os.chdir(path)
    if os.path.exists(dir) is False and bool == "Schedule":
        os.makedirs(dir) and os.chdir(os.getcwd(), dir)
        # workspaceValue = False
        os.chdir(os.path.join(path, dir))
        currentWorkspace = os.getcwd()
        print("worskspace for", dir, " at", currentWorkspace, dir, " has been created")

    elif os.path.exists(dir) and bool != "Schedule":

        os.chdir(os.path.join(path, dir))
        currentWorkspace = os.getcwd()

        print("workspace not From Scrath is OK at", currentWorkspace)
        # currentWorkspace = os.chdir(os.path.join(path, dir))
        # os.rmdir(currentWorkspace)
        # workspaceValue = False
    else:
        print("you should erase the workspace ", dir, " before \n The script will stop running .....")
        return
    return currentWorkspace;

def writeOnFileSync(ini,runofheday,booldbuild):
    #os.chdir(ini['SCRIPTPARAM']['PathFolderBuild'])
    pathRSYNC=ini['SCRIPTPARAM']['PathFolderBuild']+ini['SCRIPTPARAM']['BuildNameFolder']+ini['SCRIPTPARAM']['PATH_SOURCE_RSYNC']
    localmachineTest={
        'BUILD': booldbuild,
        'RUNID': runofheday,
        'PATHOFBUILD':str( pathRSYNC),
        'PROJECTID': ini['PROJECTPARAM']['ProjectID'],
	'SUITEID' : ini['PROJECTPARAM']['SuiteID'],
	'RSYNCDEST': ini['SCRIPTPARAM']['PATH_DEST_RSYNC'],
	'HOSTNAME': ini['SCRIPTPARAM']['ssh']['hostname'],
	'API': ini['SCRIPTPARAM']['Testrail']['apiClient'],
	'USER':ini['SCRIPTPARAM']['Testrail']['user'],
	'PASSWORD':ini['SCRIPTPARAM']['Testrail']['password']	
    }	
    path=ini['SCRIPTPARAM']['PATH_TO_YML'] +"localmachineTest.yml"
    with open(path, 'w') as yaml_file:
        yaml.dump(localmachineTest, yaml_file, default_flow_style=False)


def mainBuild(ini):
    args = argumentParsing()

    #currentPath = ini['SCRIPTPARAM']['PathFolderBuild']

    #createdWorkspace = createWorkspace(currentPath, ini['SCRIPTPARAM']['BuildNameFolder'],
    #                                   args.Boolean_From_scratch_or_Not)

    #os.chdir(createdWorkspace)
    os.chdir('/builds/' + os.environ.get('CI_PROJECT_PATH') + '/build/')

    # cree le Plan & Run ou jRajouté le Run sur l'ancien precedent paln
    # =>OK eturn planOfTheWeek, runOfTheDay,suiteId;
    planOfTheWeek, runIOftheDay = createRunWithPlan(ini['PROJECTPARAM']['ProjectID'], args.Boolean_From_scratch_or_Not,
                                                    ini['PROJECTPARAM']['SuiteID'])
    # verifié si c'est from Scrath ou pas et avoir liste de case avant####corrigé bug ou cela ne doit pas crée de plan lorsque pas schedule
    # =>

    listCmd = getCommandFromTestRail(ini, args)

    #listCmd = fromScratchOrNot(args.Boolean_From_scratch_or_Not, listCmd, runIOftheDay)
    listCmdLocal, listCmdBuild, listCmdTest = whichScript(listCmd)

    listValidation=[True]*len(listCmdBuild)
    listtest=[False]*len(listCmdBuild)
    i=0
    for cmd in listCmdBuild:
         i=i+1
         statuValue = mainCommandV2(cmd, runIOftheDay)
         listtest[i-1]=statuValue
         if statuValue is False:
             #writeOnFileSync(ini, runIOftheDay, False)
             break

    #if listtest== listValidation:
         #writeOnFileSync(ini,runIOftheDay,True)
    #else:
         #writeOnFileSync(ini, runIOftheDay, False)

if __name__ == '__main__':
    data = yaml.safe_load(open('ini.yml')) #Changer avec url du ini.yml dans le projet
    ################################ID of Testrail###############################################
    #client = APIClient(data['SCRIPTPARAM']['Testrail']['apiClient'])
    #client.user = data['SCRIPTPARAM']['Testrail']['user']
    #client.password = data['SCRIPTPARAM']['Testrail']['password']
    client = APIClient(os.environ.get('TESTRAIL_APICLIENT'))
    client.user = os.environ.get('TESTRAIL_USER')
    client.password = os.environ.get('TESTRAIL_PASSWORD')
    ############################################################################################
    mainBuild(data)