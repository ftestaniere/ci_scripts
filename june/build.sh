#!/bin/bash

# On cree le dossier qui va contenir le build
mkdir /builds/$CI_PROJECT_PATH/build
chmod 777 /builds/$CI_PROJECT_PATH/build

# On verifie si on est sur un merge ou un schedule
if [ $CI_PIPELINE_SOURCE = "schedule" ]; then
    if [ $SCHEDULE_TYPE = "weekly" ]; then
        # Build from scratch
        echo 'weekly schedule pipeline'
        # Build
        python3 /home/baylibreci/tools/build.py Schedule
        # Copie vers le dossier weekly du serveur
        rsync -azP /builds/$CI_PROJECT_PATH/build/ $SSH_USER@$SSH_HOSTNAME:/var/www/html/builds/$CI_PROJECT_NAME/weekly/
    fi
    if [ $SCHEDULE_TYPE = "nightly" ]; then
        # Build from previous nightly
        echo 'nighty schedule pipeline'
        # rsync le build nightly
        # Build
        python3 /home/baylibreci/tools/build.py Schedule
        # Copie vers le dossier nightly du serveur
        rsync -azP /builds/$CI_PROJECT_PATH/build/ $SSH_USER@$SSH_HOSTNAME:/var/www/html/builds/$CI_PROJECT_NAME/nightly/
    fi
fi
if [ $CI_PIPELINE_SOURCE = "merge" ]; then
    # Build from nightly
    echo 'merge pipeline'
    # rsync le build nightly
    # Build
    python3 /home/baylibreci/tools/build.py Schedule
    # Copie vers le dossier patch du serveur
    ssh $SSH_USER@$SSH_HOSTNAME "mkdir -p /var/www/html/builds/$CI_PROJECT_NAME/patch/$CI_COMMIT_SHORT_SHA"
    rsync -azP /builds/$CI_PROJECT_PATH/build/ $SSH_USER@$SSH_HOSTNAME:/var/www/html/builds/$CI_PROJECT_NAME/patch/$CI_COMMIT_SHORT_SHA/
fi


# TEST ONLY MANUAL TRIGGERED PIPELINE
if [ $CI_PIPELINE_SOURCE = "push" ]; then
    python3 /home/baylibreci/tools/build.py Schedule
    ssh $SSH_USER@$SSH_HOSTNAME "mkdir -p /var/www/html/builds/$CI_PROJECT_NAME/patch/$CI_COMMIT_SHORT_SHA"
    rsync -azP /builds/$CI_PROJECT_PATH/build/ $SSH_USER@$SSH_HOSTNAME:/var/www/html/builds/$CI_PROJECT_NAME/patch/$CI_COMMIT_SHORT_SHA/
fi

                
    
