#!/bin/bash

# On verifie si on est sur un merge ou un schedule
if [ $CI_PIPELINE_SOURCE = "schedule" ]; then
    if [ $SCHEDULE_TYPE = "weekly" ]; then        
        export BUILD_URL=http://$SSH_HOSTNAME/builds/$CI_PROJECT_NAME/weekly/
    fi
    if [ $SCHEDULE_TYPE = "nightly" ]; then
        export BUILD_URL=http://$SSH_HOSTNAME/builds/$CI_PROJECT_NAME/nightly/
    fi
fi
if [ $CI_PIPELINE_SOURCE = "merge" ]; then
    export BUILD_URL=http://$SSH_HOSTNAME/builds/$CI_PROJECT_NAME/patch/$CI_COMMIT_SHORT_SHA/
fi


# TEST ONLY MANUAL TRIGGERED PIPELINE
if [ $CI_PIPELINE_SOURCE = "push" ]; then
    export BUILD_URL=http://$SSH_HOSTNAME/builds/$CI_PROJECT_NAME/patch/$CI_COMMIT_SHORT_SHA/
fi

                
    